<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <?php
        include("connection.php");
        $id = $_GET['updateid'];
        $sql_select = "SELECT * FROM `phone contact` WHERE id = $id";
        $result_select = mysqli_query($con, $sql_select);
        $row = mysqli_fetch_assoc($result_select);
    ?>
    <div class="container">
        <form action="#" method="POST" role="form" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="" class="form-label">Phone Number</label>
                <input type="text" class="form-control" name="Phone_Number" value="<?php echo $row['Phone_Number']; ?>">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">First Name</label>
                <input type="text" class="form-control" name="First_Name" value="<?php echo $row['First_Name']; ?>">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Last Name</label>
                <input type="text" class="form-control" name="Last_Name" value="<?php echo $row['Last_Name']; ?>">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">URL Facebook Social</label>
                <input type="text" class="form-control" name="url_fb_social" value="<?php echo $row['url_fb_social']; ?>">
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Profile Picture url</label>
                <input type="text" class="form-control" name="profile_picture" value="<?php echo $row['Profile_Picture']; ?>">
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-danger" name="submit">Update</button>
                <button class="btn btn-danger"><a href="read.php">Read</a></button>
                
            </div>
        </form>
    </div>
   
    <?php
        if(isset($_POST['submit'])){
            $phone = $_POST['Phone_Number'];
            $firstname = $_POST['First_Name'];
            $lastname = $_POST['Last_Name'];
            $url = $_POST['url_fb_social'];
            $profile = $_POST['profile_picture'];

            // Update record in the database
            $sql_update = "UPDATE `phone contact` SET Phone_Number='$phone', First_Name='$firstname', Last_Name='$lastname', url_fb_social='$url', Profile_Picture='$profile' WHERE id=$id";
            $result_update = mysqli_query($con, $sql_update);

            if($result_update){
                echo "<script>alert('Update Successful');</script>";
                header('Location: read.php');
                exit(); // Terminate script after redirect
            }
            else{
                echo "<script>alert('Failed');</script>";
            }
        }
        $con->close();
    ?>
</body>
</html>
