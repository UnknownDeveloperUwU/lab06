<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Contacts</title>
 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>View Contacts</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Phone Number</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>URL</th>
                    <th>Profile Picture</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
               
            </thead>
            <tbody>
            <?php
            include("connection.php");

            // Retrieve data from the database
            $sql = "SELECT * FROM `phone contact`"; // Ensure table name matches the actual table name in your database
            $result = $con->query($sql);

            if ($result === false) {
                echo "Error: " . $con->error;
            } else {
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $id = $row['id'];
                        $phone = $row['Phone_Number']; // Update column names to match your database
                        $firstname = $row['First_Name'];
                        $lastname = $row['Last_Name'];
                        $url = $row['url_fb_social'];
                        $profile = $row['Profile_Picture'];
                        echo "<tr>
                                <td>$id</td>
                                <td>$phone</td>
                                <td>$firstname</td>
                                <td>$lastname</td>
                                <td>$url</td>
                                <td><img src='$profile' height='40px'/></td>
                                <td><a href='update.php?updateid=$id'>Update</a></td>
                                <td><a href='delete.php?delete=$id'>Delete</a></td>
                              </tr>";
                    }
                } else {
                    echo "<tr><td colspan='8'>No records found</td></tr>";
                }
            }
            $con->close();
            ?>
            
            </tbody>
        </table>
        <a href="create.php">create</a>
    </div>
</body>
</html>
