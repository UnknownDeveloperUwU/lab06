<?php
    $server = "localhost";
    $username = "root";
    $password = "";
    $database = "connection";

    $con = new mysqli($server, $username, $password, $database, 3306);

    if($con->connect_error){
        echo "<h3>Error database connection!</h3>";
    }
    else{
        echo "<h3>Connected to database successfully!</h3>";
    }
?>