<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    >
</head>
<body>
    <?php
        include("connection.php");
    ?>
    <div class="container">
        <form action="#" method="POST" role="form" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="" class="form-label">Phone Number</label>
            <input type="tel" class="form-control" id="" placeholder="Enter your Phone Number" name="Phone_Number">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">First Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your First Name" name="First_Name">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your Last Name" name="Last_Name">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Url facebook socail</label>
            <input type="text" class="form-control" id="" placeholder="Enter your facebook url" name="url_fb_social">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Profile picture url</label>
            <input type="text" class="form-control" id="" placeholder="Enter your profile picture link" name="Profile_Picture">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-danger" name="create">Submit</button>
        </div>
        </form>
    </div>
    <div>
        <button><a href="read.php">Read</a></button>
    </div>
    <?php
include("connection.php");


if(isset($_POST['create'])){
    
    $phone = $_POST['Phone_Number'];
    $firstname = $_POST['First_Name'];
    $lastname = $_POST['Last_Name'];
    $url = $_POST['url_fb_social'];
    $profile = $_POST['Profile_Picture'];

    
    $sql_contact = "INSERT INTO `phone contact` (Phone_Number, First_Name, Last_Name, url_fb_social, Profile_Picture) VALUES (?, ?, ?, ?, ?)";
    $stmt_contact = $con->prepare($sql_contact);
    $stmt_contact->bind_param("issss", $phone, $firstname, $lastname, $url, $profile);

    if($stmt_contact->execute()){
        echo "<script>alert('Contact added successfully');</script>";
    } else {
        echo "<script>alert('Failed to add contact');</script>";
    }

    
    $stmt_contact->close();
}

$con->close();
?>

</body>
</html>
